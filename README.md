# Stock
Full Stack application to view and search stocks

## Stack Information

- Frontend - React.js
- Backend - Node.js, Express
- Database - MySQL/ MariaDB
- API - Alphavantage

## Installation Instructions

1. Run `npm install` to install backend dependencies.
2. Run `npm run client-install` to install frontend dependencies.
3. Run `npm run dev` to start development server.
4. Stocks will be available at [http://localhost:8080/](http://localhost:8080/)

## Git 

clone url - git clone https://modimrugesh1910@bitbucket.org/modimrugesh1910/smallcase-stock-app.git